const Product = require('../models/products')
const Cart = require('../models/cart');


exports.addToCart = (req, res , next)=>
{
    const id = req.params.id;
    Cart.findOne({where:{ userId : req.session.id , productId : id}})
    .then(result =>{
        result.quantity += 1;
        result.save();
        res.redirect('/')
    })
    .catch(err =>{
        Cart.create({
        quantity: 1,
        userId : req.session.id,
        productId: id
        
        })
        .then(result=>{res.redirect('/')}

        )
    
    })
    


}

exports.delete = (req , res , next) =>{
    const id = req.params.id;
    Cart.findOne({where:{productId:id , userId : req.session.id}})
    .then(result=>
        {return result.destroy()})
    .then(result =>{
        res.redirect('/cart')
    })
    .catch(err=>{console.log(err)})
}



exports.letsorder = (req , res , next ) => {

    const s = Cart.findAll({where:{userId:req.session.id}})
    .then(result=>{
        
        for(let item in result) {
            result[item].order = true
            result[item].save();
            }
        console.log(result)
        

    })
    .then(result =>{
        return req.session.destroy()
    })
    .then(result =>{
        res.redirect('/')
    })

}
// exports.showCart = (req , res , next)=>
// {
//     Cart.
// }

































































