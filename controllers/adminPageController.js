const bodyParser = require('body-parser');
const app = require('../routes/alphaPages');
const Product = require('../models/products');
const Cart = require('../models/cart')
app.use(bodyParser.urlencoded({extended:false}));



exports.addPage = (req ,res ,next)=>
{
    // req.session.destroy(err=>{console.log(err)})
    res.render('adminAdd.ejs' , {
        pageTitle:'Add',
        path:'admin/add'
    });
};

exports.adminPage =(req , res ,next) =>
{
    console.log(req.session.id);
    Product.findAll()
    .then(result=>{
        // console.log(result[0])
        res.render('adminPage.ejs' , {
        pageTitle:'Admin',
        path:'admin',
        products : result

    });
    })
    .catch(err=>{console.log(err)});
   
};

exports.addProduct = (req , res , next)=>
{
    const product = Product.create(
    {
        title : req.body.title ,
        imageurl:req.body.imageurl ,
        description: req.body.description ,
        price : req.body.price,

    })
    .then(result =>{
        res.redirect('/admin')
    })
    .catch(err=>{console.log(err)});

};

exports.editProduct = (req , res , next)=>
{
    let id = req.params.id;
    // console.log(id);
    Product.findOne({where : {id : id}})
    .then(result=>
        {
            // console.log(result[0])
            res.render('adminEdit.ejs' , {
            pageTitle:"Editing",
            path:null,
            product:result 
        })
        })
    .catch(err=>
        {
            console.log(err)
        });
    
}
exports.doEditProduct = (req , res , next)=>
{
  
    let id = req.params.id;
    let prod = req.body;
    Product.findOne({where : {id : id}})
    .then(product =>{
        
        product.title = (prod.title != "") ? prod.title : product.title
        product.imageurl = (prod.imageurl != "")? prod.imageurl : product.imageurl
        product.price = (prod.price != "" )? prod.price : product.price
        product.description = (prod.description != "") ? prod.description : product.description
        return product.save()
    })
    .then(result => {
        console.log("edited successfully")
        res.redirect('/admin');
    })
    .catch(err=>{console.log(err)})
    
}

exports.deleteProduct = (req,res,next)=>
{
    let id = req.params.id;
    Product.findOne({where : {id : id}})
    .then(result=>
    {
        return result.destroy()
    })
    .then(result =>
    {
        return Cart.findAll({where:{productId : id}})
        
    })
    .then(result=>{
        for(let item in result)result[item].destroy()
        res.redirect('/admin');
    })
    
    .catch(err=>{console.log(err)})

}
