const { render } = require("ejs");
const Product = require('../models/products');
const Cart = require('../models/cart')
const Sequelize = require('sequelize');
const { INTEGER } = require("sequelize");
const Op = Sequelize.Op
exports.indexPage = (req , res , next)=>{
    
    Product.findAll().then(result=>{
        
        res.render('index.ejs' , {
        pageTitle : 'Home',
        path : 'home',
        products : result
        });
    })
    .catch(err=>{console.log(err)})
    
};

exports.cartPage = (req ,res , next)=>{
    
    let carty
    Cart.findAll({where:{userId : req.session.id}})
    .then(result =>{
        carty = result
        let prods = new Array
        for(let item in result){
            prods.push(result[item].productId)
        }

        // quantity.reverse(); 
        return Product.findAll({where:{id :{[Op.in]: prods}}})
        })
    .then(result =>{
        let cost = 0
        for(let item in result){
            for(let it in carty){
                if(carty[it].productId === result[item].id) result[item].quantity = carty[it].quantity
            }
            let num = result[item].price * result[item].quantity
            cost = cost + num
        }

        res.render('cartPage.ejs' , {
        pageTitle:'Cart',
        path:'cart',
        products : result,
        cost : cost
            })
        
    })
    .catch(err =>{
        res.render('cartPage.ejs' , {
            pageTitle:'Cart',
            path:'cart',
            products : [],
            cost : 0
            })
    })
    
}