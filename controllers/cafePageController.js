const Product = require('../models/products');

exports.showCafePage = (req ,res , next)=>{

    const id = req.params.id;
    Product.findOne({where:{id : id}})
    .then(result=>{
            
            res.render('cafePage.ejs' , {
            pageTitle : result.title,
            path : null,
            product : result

        })
        

    })
    .catch(err=>{
        console.log(err)
    })
    
}