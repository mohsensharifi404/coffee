const db = require('../helpers/mysql');
const Sequelize = require('sequelize');

const Products = db.define('products', {
    // Model attributes are defined here
    id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        // unique: 'compositeIndex',
        autoIncrement: true,
        primaryKey: true

    },
    title: {
        type: Sequelize.STRING,
        allowNull: false,

    },
    price: {
        type: Sequelize.DOUBLE,
        allowNull: false
    },
    imageurl: {
        type: Sequelize.STRING,
        allowNull: false
        
    },
    description: {
        type: Sequelize.TEXT,
        allowNull: false
    }
    }, {
    // Other model options go here
    });


        // return db.execute('INSERT INTO firsttry (title , price , imageurl , description) VALUES ( ? , ? , ? , ? )',
        // [this.title , Number(this.price) , this.imageurl , this.description]
        //  )
module.exports = Products;