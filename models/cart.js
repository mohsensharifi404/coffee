const db = require('../helpers/mysql');
const Sequelize = require('sequelize');

const Cart = db.define('cart-items' ,{
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
      },
      
    quantity: Sequelize.INTEGER,
    
    userId: {
        type:Sequelize.STRING,
        allowNull: false
    },
    
    productId:{
        type :Sequelize.INTEGER,
        defaultValue: false
    },
    order:{
        type :Sequelize.BOOLEAN,
        defaultValue: false,

    }
})

module.exports = Cart;



























