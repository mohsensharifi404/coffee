const express = require('express');
const app = express.Router();
const cafePageController = require('../controllers/cafePageController');
app.get('/:id' ,cafePageController.showCafePage);

module.exports = app;