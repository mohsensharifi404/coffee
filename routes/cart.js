const cartController = require('../controllers/cartController')
const express = require('express');
const app = express.Router();

app.get('/order' , cartController.letsorder);

app.get('/delete/:id' , cartController.delete);

app.get('/:id' , cartController.addToCart);





module.exports = app;