
const express = require('express');
const app = express.Router();
const adminPageController = require('../controllers/adminPageController');

app.get('/' , adminPageController.adminPage);
app.get('/add' , adminPageController.addPage);
app.post('/add' , adminPageController.addProduct);
app.get('/edit/:id' , adminPageController.editProduct);
app.post('/edit/:id' , adminPageController.doEditProduct);
app.get('/delete/:id' , adminPageController.deleteProduct);

module.exports = app;










