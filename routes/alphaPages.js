const alphaPageController = require('../controllers/alphaPageController')

const express = require('express');


const app = express.Router();

app.get('/' , alphaPageController.indexPage);

app.get('/cart' , alphaPageController.cartPage);

module.exports = app;

